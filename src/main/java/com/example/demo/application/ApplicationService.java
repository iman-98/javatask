package com.example.demo.application;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class ApplicationService {
    private final ApplicationRepository applicationRepository;

    public ApplicationService(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public Page<Application> findBy(Long id, Boolean seen, Pageable paging) {
            if (seen == null) return applicationRepository.findByJobId(id, paging);
            else return applicationRepository.findAllBySeenAndJobId(seen, id, paging);
    }
}
