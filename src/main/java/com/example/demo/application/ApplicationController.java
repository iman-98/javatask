package com.example.demo.application;

import com.example.demo.job.Job;
import com.example.demo.job.JobRepository;
import com.example.demo.job.JobService;
import com.example.demo.user.models.User;
import com.example.demo.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/api/job-application")
public class ApplicationController {
    private final UserRepository userRepository;
    private final JobRepository jobRepository;
    private final ApplicationRepository applicationRepository;
    private final ApplicationService applicationService;

    @Autowired
    public ApplicationController(JobService jobService, UserRepository userRepository, JobRepository jobRepository, JobRepository jobRepository1, ApplicationRepository applicationRepository, ApplicationService applicationService) {
        this.userRepository = userRepository;
        this.jobRepository = jobRepository1;
        this.applicationRepository = applicationRepository;
        this.applicationService = applicationService;
    }

    @PostMapping("/apply/{id}")
    public ResponseEntity<Object> applyToJob(@PathVariable Long id) {
        String username = User.currentUserName();
        Optional<User> userOptional = userRepository.findByUsername(username);
        Job job = jobRepository.findJobById(id);
        if (userOptional.isEmpty()) return ResponseEntity.notFound().build();
        if (job.getStatus().equals(false))
            return new ResponseEntity<>("Job is closed", HttpStatus.NOT_ACCEPTABLE);
        User user = userOptional.get();
        Application application = new Application();
        application.setUser(user);
        application.setJob(job);
        application.setSeen(false);
        applicationRepository.save(application);
        return new ResponseEntity<>(application, HttpStatus.CREATED);
    }

    @GetMapping("/list/{jobId}")
    public Object getJobApplications(
            @PathVariable Long jobId,
            @RequestParam(required = false) Boolean seen,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        String username = User.currentUserName();
        Optional<User> userOptional = userRepository.findByUsername(username);
        Job job = jobRepository.findJobById(jobId);
        if (userOptional.isEmpty()) return null;
        User user = userOptional.get();
        if (job.getUser().getId().equals(user.getId())) {
            return applicationService.findBy(jobId, seen, paging);
        }
        return null;
    }

    @GetMapping("/application/{id}")
    public Object getJobApplication(@PathVariable Long id) {
        Application application = applicationRepository.findApplicationById(id);
        if (application.getSeen().equals(false))
            application.setSeen(true);
        applicationRepository.save(application);
        return new ResponseEntity<>(application, HttpStatus.OK);
    }

}
